package com.kshrd.ams.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

public class Article {
	
	//@NotNull
	@NotNull
	private int id;
	@NotEmpty
	@Size(min=10,max=30)
	private String title;
//	@NotBlank(message="cannot empty")
	@NotBlank(message="cannot empty")
	private String description;
	private String createdDate;
	@NotBlank
	private String author;
	private String thumnail;

	public Article() {}
	
	public Article(int id, String title, String description, String createdDate, String author, String thumnail) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.createdDate = createdDate;
		this.author = author;
		this.thumnail = thumnail;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getThumnail() {
		return thumnail;
	}

	public void setThumnail(String thumnail) {
		this.thumnail = thumnail;
	}
	
	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", description=" + description + ", createdDate="
				+ createdDate + ", author=" + author + ", thumnail=" + thumnail + "]";
	}
}
