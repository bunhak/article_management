package com.kshrd.ams.repository;

import com.github.javafaker.Faker;
import com.kshrd.ams.model.Article;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class ArticleRepositoryImpl implements ArticleRepository {

	private ArrayList<Article> articles = new ArrayList<>();
	
	public ArticleRepositoryImpl() {
		Faker faker = new Faker();
		for(int i=1; i<=10; i++)
			articles.add(new Article(i, faker.book().title(), faker.book().title(), new Date().toString(),faker.name().fullName(),faker.internet().image()));
	}
	
	@Override
	public void insert(Article article) {
		articles.add(article);
	}

	@Override
	public Article findOne(int id) {
		for(Article a : articles) {
			if (a.getId() == id) {
				return a;
			}
		}
		return null;
	}

	@Override
	public List<Article> findAll() {
		return articles;
	}

	@Override
	public void update(Article article) {
		for(int i=0;i<articles.size();i++) {
			if(articles.get(i).getId() == article.getId()) {
				articles.get(i).setTitle(article.getTitle());
				articles.get(i).setDescription(article.getDescription());
				articles.get(i).setAuthor(article.getAuthor());
			}
		}
	}

	@Override
	public void delete(int id) {
		for(Article a:articles) {
			if(a.getId()==id) {
				articles.remove(a);
				return;
			}
		}
	}

	
}
