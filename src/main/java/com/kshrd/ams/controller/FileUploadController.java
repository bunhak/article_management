package com.kshrd.ams.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class FileUploadController {
    @GetMapping("/upload")
    public String upload(){
        return "upload";
    }

    @PostMapping("/upload")
    public String saveFile(@RequestParam("file") List<MultipartFile> files){
        String serverPath ="\\Users\\KPS_014\\Desktop\\Upload";
        if (!files.isEmpty()) {
            files.forEach(file -> {
                String fileName = UUID.randomUUID() + "." + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
                try {
                    Files.copy(file.getInputStream(), Paths.get(serverPath, fileName));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

        }
        return "upload";
    }
}
