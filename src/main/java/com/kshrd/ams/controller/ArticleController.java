package com.kshrd.ams.controller;

import com.kshrd.ams.model.Article;
import com.kshrd.ams.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class ArticleController {
	
	@Autowired
	private ArticleService articleService;
	
	@RequestMapping(value= {"/","/home","/index"})
	public String home() {
		return "redirect:/article";
	}

	@GetMapping("/article")
	public String article(ModelMap model) {
		List<Article> articles = articleService.findAll();
//		System.out.println(articles);
		model.addAttribute("articles", articles);
		return "article";
	}
	
	@GetMapping("/article/{id}")
	public String articleOne(@PathVariable("id") int id, ModelMap model) {
		
		model.addAttribute("article", articleService.findOne(id));
		return "article_detail";
	}
	
	@GetMapping("/test")
	public String articleOneaaa(@RequestParam("a_id") int id, ModelMap model) {
		
		model.addAttribute("article", articleService.findOne(id));
		return "article_detail";
	}
	
	@GetMapping("/add")
	public String addArticle(ModelMap m) {
		m.addAttribute("formAdd", true);
		m.addAttribute("article", new Article());
		return "add";
	}
	
	@PostMapping("/add")
	public String saveArticle(@Valid @ModelAttribute Article article, BindingResult result, MultipartFile file ,ModelMap m) {
		if (file == null) {
			return null;
		}
		File path = new File("/images");
		if (!path.exists()){
			path.mkdir();
		}
		if(result.hasErrors()) {
			m.addAttribute("formAdd", true);
			m.addAttribute("article", article);
			return "add";
		}
		if(!file.isEmpty()){
			String fileName=file.getOriginalFilename();
			try {
				fileName= UUID.randomUUID()+"."+fileName.substring(fileName.lastIndexOf('.'));
				Files.copy(file.getInputStream(), Paths.get("/images",fileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
			article.setThumnail("/image/"+fileName);
		}
		else{
			article.setThumnail("/image/"+"a.jpg");
		}
		article.setCreatedDate(new Date().toString());
		System.out.println(article);
		articleService.insert(article);
		return "redirect:/article";
	}
	
	@GetMapping("/update/{id}")
	public String update(@PathVariable("id") int id, ModelMap model) {
		model.addAttribute("formAdd", false);
		model.addAttribute("article", articleService.findOne(id));
		return "add";
	}
	
	@PostMapping("/update")
	public String saveChanged(@ModelAttribute Article article) {
		articleService.update(article);
		return "redirect:/article";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") int id) {
		articleService.delete(id);
		return "redirect:/article";
	}
}
